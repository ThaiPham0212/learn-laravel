{{--<form action="/post" method="POST">--}}
{{--    @csrf--}}

{{--    <p>Username</p>--}}
{{--    <div>--}}
{{--        <input type="text" name="username">--}}
{{--    </div>--}}

{{--    <p>Password</p>--}}
{{--    <div>--}}
{{--        <input type="password" name="password">--}}
{{--    </div>--}}
{{--    <br>--}}
{{--    <div>--}}
{{--        <button type="submit">Login</button>--}}
{{--    </div>--}}
{{--</form>--}}


{{--<form action="/post" method="POST">--}}
{{--    @csrf--}}

{{--    <div>--}}
{{--        Name: <input type="text" name="products[][name]">--}}
{{--        Price: <input type="text" name="products[0][price]">--}}
{{--    </div>--}}
{{--    <div>--}}
{{--        Name: <input type="text" name="products[][name]">--}}
{{--        Price: <input type="text" name="products[1][price]">--}}
{{--    </div>--}}

{{--    <br>--}}

{{--    <div>--}}
{{--        <button type="submit">Submit</button>--}}
{{--    </div>--}}
{{--</form>--}}



{{--<form action="/post?id=1" method="POST">--}}
{{--    @csrf--}}

{{--    <div>--}}
{{--        <button type="submit">Submit</button>--}}
{{--    </div>--}}
{{--</form>--}}



{{--<form action="/post" method="POST">--}}
{{--    @csrf--}}

{{--    <div>--}}
{{--        <button type="submit">Submit</button>--}}
{{--    </div>--}}
{{--</form>--}}



{{--<script src="https://code.jquery.com/jquery.min.js"></script>--}}
{{--<script>--}}
{{--    $('form').submit(function (e) {--}}
{{--        e.preventDefault();--}}

{{--        $.ajax({--}}
{{--            url: '/post',--}}
{{--            type: 'POST',--}}
{{--            data: {--}}
{{--                _token: $('input[name=_token]').val(),--}}
{{--                user: {--}}
{{--                    name: 'Phạm Hồng Thái',--}}
{{--                    age: 18--}}
{{--                }--}}
{{--            }, success: function(res) {--}}
{{--                console.log(res);--}}
{{--            }--}}
{{--        });--}}
{{--    });--}}
{{--</script>--}}



{{--<form action="/post" method="POST">--}}
{{--    @csrf--}}

{{--    <p>Username</p>--}}
{{--    <div>--}}
{{--        <input type="text" name="username" value="{{ old('username') }}">--}}
{{--    </div>--}}

{{--    <p>Password</p>--}}
{{--    <div>--}}
{{--        <input type="password" name="password">--}}
{{--    </div>--}}

{{--    <br>--}}

{{--    <div>--}}
{{--        <button type="submit">Login</button>--}}
{{--    </div>--}}
{{--</form>--}}

@inject('cookie', 'Illuminate\Support\Facades\Cookie')

<p>Hello, {{ $cookie->get('name') }}</p>

<form action="/post" method="POST">
    @csrf

    <div>
        <button type="submit">Set cookie</button>
    </div>
</form>



<form action="/post" method="POST" enctype="multipart/form-data">
    @csrf

    <div>
        <p>Avatar</p>
        <input type="file" name="avatar">
    </div>

    <br>

    <div>
        <button type="submit">Upload</button>
    </div>
</form>
