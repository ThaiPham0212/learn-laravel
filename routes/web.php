<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\UserController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\ResponseFactory;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/foo', function() {
//    return 'Hello world';
//});
//
//Route::post('create', function() {
//    return 'Created';
//});
//
//Route::redirect('/here', '/there');
//
//Route::view('/welcome', 'welcome', ['name' => 'Phạm Hồng Thái']);
//
//Route::get('/user/{id}/post/{post}', function($id, $idPost) {
//    return "This is post $idPost of user $id";
//});
//
////Route::get('post/{id?}', function($id = 1) {
////    return "Post $id";
////});
//
//Route::get('user/{name}', function($name) {
//    //
//})->where('name', '[A-Za-z]+');
//
//Route::get('age/{age}', function ($age) {
//    return $age;
//})->middleware('checkAge')->middleware('auth');
//
//Route::group(['middleware' => ['web']], function () {
//    //
//});
//
//
//
//Route::get('/home', [HomeController::class, 'index'])->name('home');
//
//Route::get('/showHome', showHomeController::class);
//
//Route::get('/showHome/{page}', 'App\Http\Controllers\showHomeController');
//
////Route::resource('posts','App\Http\Controllers\PostController');
//
//Route::get('/post', function () {
//    return 'Body post';
//})->middleware('role:admin');


//Route::resources([
//    'posts' => 'App\Http\Controllers\PostController',
//    'categories' => 'App\Http\Controllers\CategpryController',
//    'photos' => 'App\Http\Controllers\PhotoController'
//]);
//
//Route::resource('photos', 'PhotoController')->names([
//    'create' => 'photos.build' // 'name_method' => 'new_name_resource_route'
//]);
//
//Route::resource('categories', 'CategoryController')->parameters([
//    'categories' => 'category' //'name_resource' => 'new_name_parameter'
//]);

Route::get('/view', function (){
    return view('home', ['name' => 'Pham Thai']);
});
Route::get('/profile', function () {
    return view('profile');
});

Route::get('/dashboard', function () {
    return view('dashboard');
});

Route::put('user/{id}', [UserController::class, 'update'])->name('user.update');

Route::get('/foo/bar', function (Request $request) {
    return $request->path();
});

//Route::fallback(function (Request $request) {
//    return dd([
//        $request->url(),
//        $request->fullUrl()
//    ]);
//});

Route::get('/request-method', function (Request $request) {
    echo 'Current method HTTP: ' . $request->method() . '<br>';

    if ($request->isMethod('get')) {
        echo 'This is GET method HTTP';
    }
});

Route::get('/', [FormController::class, 'show'])->name('show');

Route::post('/post', [FormController::class, 'post']);

// response array
Route::get('/response', function () {
    return [1, 2, 3];
});

// response object
Route::get('/home', function () {
//    return response('Hello World', 200)->header('Content-Type', 'text/plain');

    return response('123')->header('Content-Type', 'text/plain')
        ->header('X-Header-One', 'Header Value')
        ->header('X-Header-Two', 'Header Value');

//    return response('123')
//        ->withHeaders([
//            'Content-Type' => 'text/plain',
//            'X-Header-One' => 'Header Value',
//            'X-Header-Two' => 'Header Value',
//        ]);
});

// return json data from $user
Route::get('/user/{user}', function (User $user) {
    return $user;
})->name('user-one');

// Cache control middleware
Route::middleware('cache.headers:public;max_age=2628000;etag')->group(function () {
    Route::get('privacy', function () {
        return response('Hello World')->cookie(
            'name', 'value', '12'
        );
    });

    Route::get('terms', function () {
        $cookie = cookie('name', 'value', '1');

        return response('Hello World')->cookie($cookie);
    });
});

// redirect URI
Route::get('dashboard', function () {
    return redirect('home');
});

// redirect route
Route::get('dashboard/user', function () {
    return redirect()->route('user-one', ['user' => 1]);
});

// redirect controller action
Route::get('/home/user', function() {
    return redirect()->action([HomeController::class, 'show']);
});
// redirect
Route::get('/home', [HomeController::class, 'show']);

// redirect to external domain
Route::get('/gg', function (){
    return redirect()->away('https://www.google.com');
});

// redirect with flash session
Route::get('/session', function() {
    return redirect('session/home')->with('name', 'Pham Thai');
});

Route::get('/session/home', function() {
    return view('home.dashboard');
});

// download
Route::get('/download', function() {
    return response()->download('../resources/views/welcome.blade.php', 'home.blade.php', ['X-Header-One' => 'Header value 1',
        'X-Header-Two' => 'Header value 2']);
});

// download
Route::get('/stream', function() {
    return response()->streamDownload(function() {
        echo 'Pham Thai';
    }, 'users.txt');
});

// file response

Route::get('/image', function() {
    return response()->file('image.png');
});

// Macro
Route::get('caps/{str}', function($str) {
    return response()->caps($str);
});
