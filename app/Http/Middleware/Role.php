<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, string $role): Response
    {
//        $response = $next($request);

        if ($role != 'editor') {
            return response('Bạn không đủ quyền truy cập');
        }
        return $next($request);
    }

    public function terminate($request, $response)
    {
        // Store the session data...
    }
}
