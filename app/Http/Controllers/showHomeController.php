<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class showHomeController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        return 'Dashboard page ' . $request->page;
    }
}
