<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function show()
    {
        return view('form');
    }

    public function post(Request $request)
    {
//        dd($request->all());

//        dd($request->input('products'));

//        $request->input('products.0'); // Lấy toàn bộ thông tin sản phẩm có index "0"
//        $request->input('products.0.name'); // Lấy name của sản phẩ có index "0"
//        $request->input('products.*.name');

//        dd($request->query('id'));

//        dd($request->name);

//        return $request->input('user.name');

//        Chỉ lấy input có name là "username" và "password"
//        $request->only(['username', 'password']);
//        $request->only('username', 'password');
//        Lấy tất cả input ngoại trừ input có name là "credit_card"
//        $request->except(['credit_card']);

//        if ($request->has('name')) {
//            //
//        }

//        return back()->withInput(
//            $request->only('username')
//        );
        dd($request->all());
        Cookie::queue('name', 'Lê Chí Huy', 5);

        echo $request->cookie('name');
    }
}
