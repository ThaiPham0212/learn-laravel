<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
//        $this->middleware('auth')->only('login');
//        $this->middleware('auth')->except('logout');
        return 'Dashboard page ' . $request->page;
    }

    public function show()
    {
        return response()->json([
                'name' => 'Pham Thai',
                'status' => 'VN'
            ]);
    }

}
