<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $user;
    public function __construct(User $user){
        $this->middleware('auth');
        $this->user = $user;
    }
    public function index(){
        return 'user';
    }
    public function store(Request $request){

    }

    public function update(Request $request, $id){

    }
}
