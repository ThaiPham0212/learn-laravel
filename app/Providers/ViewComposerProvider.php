<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
class ViewComposerProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
        // Registering composer with Class
        View::composer(
            'profile', 'App\Http\View\Composers\ProfileComposer'
        );

        // Registering composer with Closure
        View::composer('dashboard', function ($view) {
            $view->with('name', 'Phạm Hồng Thái');
        });
    }
}
